sudo apt update &&
    sudo apt upgrade &&
    wget --quiet --output-document - https://updates.signal.org/desktop/apt/keys.asc | sudo apt-key add - &&
    echo "deb [arch=amd64] https://updates.signal.org/desktop/apt xenial main" | sudo tee -a /etc/apt/sources.list.d/signal-xenial.list &&
    sudo apt update &&
    sudo apt install curl emacs git gnome-session gnome-tweaks &&
    sudo apt install signal-desktop-beta &&
    curl -fsSL https://deb.nodesource.com/setup_15.x | sudo -E bash - &&
    sudo apt install nodejs &&
    sudo apt-add-repository ppa:ubuntu-mozilla-daily/ppa &&
    sudo apt update &&
    sudo apt install firefox-trunk &&
    sudo npm i http-server &&
    sudo apt update &&
    cd ~ &&
    git clone https://gitlab.com/eleanorofs/emacsd.git .emacs.d

