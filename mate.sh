sudo apt update &&
  sudo apt upgrade &&  
  sudo apt install emacs git &&
  sudo snap install node --classic &&
  sudo apt-add-repository ppa:ubuntu-mozilla-daily/ppa &&
  sudo apt update &&
  sudo apt install firefox-trunk &&
  sudo add-apt-repository ppa:ubuntubudgie/backports &&
  sudo apt update &&
  sudo apt install skippy-xd &&
  cd ~ &&
  git clone https://gitlab.com/eleanorofs/emacsd.git .emacs.d
